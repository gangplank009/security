package com.gangplank009.security.service;

import com.gangplank009.security.entity.Account;
import com.gangplank009.security.interceptor.RestTempHeaderRequestInterceptor;
import com.gangplank009.security.repository.AccountRepository;
import com.gangplank009.security.request.AuthenticationRequest;
import com.gangplank009.security.response.JWTTokenResponse;
import com.gangplank009.security.response.UserWallet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityNotFoundException;
import java.util.*;
import java.util.stream.Stream;

@Service
public class RegistrationService {

    @Value(value = "${jwt.header}")
    private String STR_AUTH;

    @Value(value = "${jwt.bearer}")
    private String STR_BEARER;

    @Autowired
    @Qualifier("reportWalletTemplate")
    private RestTemplate reportWalletTemplate;

    private AuthenticationService authenticationService;
    private AccountRepository accountRepository;
    private PasswordEncoder passwordEncoder;

    public RegistrationService(AccountRepository accountRepository,
                               PasswordEncoder passwordEncoder,
                               AuthenticationService authenticationService) {
        this.accountRepository = accountRepository;
        this.passwordEncoder = passwordEncoder;
        this.authenticationService = authenticationService;
    }

    public boolean saveNewUser(String username, String password) {
        Optional<Account> accountOptional = accountRepository.findOneByUsername(username);
        if (accountOptional.isPresent()) {
            return false;
        } else {
            Account user = new Account();
            user.setUsername(username);
            user.setPassword(passwordEncoder.encode(password));
            accountRepository.save(user);
            return true;
        }
    }

    public ResponseEntity<?> getResponseEntity(@RequestBody AuthenticationRequest request) {
        JWTTokenResponse jwtToken = authenticationService.generateJWTToken(request.getUsername(), request.getPassword());
        List<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
        Map<String, String> headers = new HashMap<>();
        headers.put(STR_AUTH, STR_BEARER + " " + jwtToken.getToken());
        headers.put("Content-Type", "application/json");
        interceptors.add(new RestTempHeaderRequestInterceptor(headers));
        reportWalletTemplate.setInterceptors(interceptors);
        ResponseEntity<UserWallet> walletResponseEntity = reportWalletTemplate.exchange(
                "/",
                HttpMethod.POST,
                null,
                UserWallet.class);
        return walletResponseEntity.getStatusCode() == HttpStatus.OK ? ResponseEntity.ok(jwtToken) : ResponseEntity.badRequest().body("Can't create wallet");
    }

    public Optional<Account> findByUsername(String username) {
        return accountRepository.findOneByUsername(username);
    }
}
