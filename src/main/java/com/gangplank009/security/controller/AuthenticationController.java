package com.gangplank009.security.controller;

import com.gangplank009.security.entity.Account;
import com.gangplank009.security.request.AuthenticationRequest;
import com.gangplank009.security.service.AuthenticationService;
import com.gangplank009.security.service.RegistrationService;
import com.gangplank009.security.validation.AccountValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for users registration and authentication
 * @author Ilsur Chubarkin
 * @version 1.1
 */

@RestController
@RequestMapping(path = "/security")
public class AuthenticationController {

    private AuthenticationService authenticationService;
    private RegistrationService registrationService;
    private AccountValidator accountValidator;

    public AuthenticationController(AuthenticationService authenticationService, RegistrationService registrationService, AccountValidator accountValidator) {
        this.authenticationService = authenticationService;
        this.registrationService = registrationService;
        this.accountValidator = accountValidator;
    }

    /**
     * Method for processing request for /login page
     * @param request contains String Account.username and String Account.password parameters
     * @return Http.Status.OK if login successfully. If login unsuccessful @{@link AuthenticationService}
     * throws the {@link EntityNotFoundException}
     */
    @RequestMapping(path = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> createCustomer(@RequestBody AuthenticationRequest request) {
        return new ResponseEntity<>(authenticationService.generateJWTToken(request.getUsername(),
                                    request.getPassword()),
                                    HttpStatus.OK);
    }

    /**
     * Method for catching {@link EntityNotFoundException} and throwing {@link HttpStatus NOT_FOUND}
     * @param ex instance of {@link EntityNotFoundException}
     * @return {@link HttpStatus OK}
     */
    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<?> handleEntityNotFoundException(EntityNotFoundException ex) {
        return new ResponseEntity<>(ex.getMessage(),
                                    HttpStatus.NOT_FOUND);
    }

    /**
     * Method for list all Account.username(s), which exist in database
     * @return JSON represents of List of String format {"username1", 'username2", "username3", etc..}
     */
    @RequestMapping(path = "/accounts", method = RequestMethod.GET)
    public ResponseEntity<?> accountsList() {
        return new ResponseEntity<>(authenticationService.getAllAccountsName(),
                                    HttpStatus.OK);
    }

    @RequestMapping(path = "/registration", method = RequestMethod.GET)
    public ResponseEntity<String> registerUser(Model model) {
        model.addAttribute("newAccountForm", new Account());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Main method for registration new {@link Account} entity
     * @param account new {@link Account} instance, contains String username and String password parameters
     * @param bindingResult contains result {@link Error}(s) of validating text in the fields registration form
     * @return {@link ResponseEntity} {@link HttpStatus}. If bindingResult contains errors,
     * then method will return List<String> with error message
     */
    @RequestMapping(path = "/registration", method = RequestMethod.POST)
    public ResponseEntity<List<String>> registerUser(@ModelAttribute("newAccountForm") Account account,
                                                     BindingResult bindingResult) {
        accountValidator.validate(account, bindingResult);
        if(bindingResult.hasErrors()){
            List<String> errorList = new ArrayList<>();
            List<ObjectError> objectErrors= bindingResult.getAllErrors();
            for(ObjectError error : objectErrors){
                errorList.add(error.getDefaultMessage());
            }
            return new ResponseEntity<>(errorList, HttpStatus.BAD_REQUEST);
        }
        registrationService.saveNewUser(account.getUsername(), account.getPassword());

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
