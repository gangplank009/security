package com.gangplank009.security.validation;

import com.gangplank009.security.entity.Account;
import com.gangplank009.security.service.RegistrationService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Simple Spring bean validator for {@link com.gangplank009.security.entity.Account} class
 * Bean validating form data for:
 * - empty of whitespaces fields
 * - login length must be from 6 to 32 characters
 * - password must be at least 8 characters
 * - fields password and confirm password must be equals
 * Text for errors contains at @file validation.properties file
 * {@see validation.properties}
 * @author Ilya Peshkin
 * @version 1.0
 */

@Component
@AllArgsConstructor
public class AccountValidator implements Validator {

    private RegistrationService registrationService;

    @Override
    public boolean supports(Class<?> aClass) {
        return Account.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Account account;
        if (o instanceof Account) {
            account = (Account) o;
        } else throw new UsernameNotFoundException("Something validating goes wrong");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "Required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "Required");
        if (account.getUsername().length() < 6 || account.getUsername().length() > 32) {
            errors.rejectValue("username", "Size.userForm.username");
        }
        if (account.getConfirmPassword().length() < 8) {
            errors.rejectValue("password", "Size.userForm.password");
        }
        if (registrationService.findByUsername(account.getUsername()).isPresent()) {
            errors.rejectValue("username", "Duplicate.userForm.username");
        }
        if (!account.getConfirmPassword().equals(account.getPassword())) {
            errors.rejectValue("confirmPassword", "Different.userForm.password");
        }
    }
}
