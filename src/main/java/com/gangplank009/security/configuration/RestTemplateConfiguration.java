package com.gangplank009.security.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

@Configuration
public class RestTemplateConfiguration {

    @Value("${url.local}")
    private String localURL;

    @Value("${port.report}")
    private String portReport;

    @Value("${urn.report.wallet}")
    private String reportWalletURN;

    @Bean
    public RestTemplate reportWalletTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory(localURL + portReport + reportWalletURN));
        return restTemplate;
    }
}
