package com.gangplank009.security;

import com.gangplank009.security.entity.Account;
import com.gangplank009.security.repository.AccountRepository;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

    private AccountRepository accountRepository;
    private PasswordEncoder passwordEncoder;

    public ApplicationStartup(AccountRepository accountRepository, PasswordEncoder passwordEncoder) {
        this.accountRepository = accountRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        accountRepository.deleteAll();
        Account account;

        for (int i = 1; i < 10; i++) {
            account = new Account();
            account.setUsername("user" + i);
            account.setPassword(passwordEncoder.encode("pass" + i));
            accountRepository.save(account);
        }
    }
}

