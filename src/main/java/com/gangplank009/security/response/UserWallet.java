package com.gangplank009.security.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * POJO class, represents account wallet
 * @author Ilsur Chubarkin
 * @version 1.0
 */

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserWallet implements Serializable {

    public UserWallet(Long id, Long money) {
        this.id = id;
        this.money = money;
    }

    private Long id;
    private Long money;

    public static UserWallet getInstance(Long number) {
        return new UserWallet(number, number);
    }
}
