package com.gangplank009.security.service;

import com.gangplank009.security.entity.Account;
import com.gangplank009.security.repository.AccountRepository;
import com.gangplank009.security.response.JWTTokenResponse;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service for authentication {@link Account} entity
 * @author Ilsur Chubarkin
 * @version 1.0
 */

@Service
@AllArgsConstructor
public class AuthenticationService {

    private AccountRepository accountRepository;
    private JwtTokenService jwtTokenService;
    private PasswordEncoder passwordEncoder;

    /**
     * @return List of all Account.username, exists in database
     */
    public List<String> getAllAccountsName() {
       List<String> result = accountRepository.findAll().stream().map(x -> x.getUsername()).collect(Collectors.toList());
        return result;
    }

    /**
     * Method for generating JWT-token.
     * @param username represents Account.username
     * @param password represents Account.password
     * @return JTW-token
     */
    public JWTTokenResponse generateJWTToken(String username, String password) {
        Optional<Account> accountOptional = accountRepository.findOneByUsername(username);
        if (accountOptional.isPresent()) {
            Account account = accountOptional.get();
            if (passwordEncoder.matches(password, account.getPassword())) {
                return new JWTTokenResponse(jwtTokenService.generateToken(account.getId(), account.getUsername()));
            } else throw new EntityNotFoundException("Account not found");
        } else throw new EntityNotFoundException("Account not found");
    }

}
