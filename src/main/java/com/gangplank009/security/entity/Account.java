package com.gangplank009.security.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Main entity class represents users account
 * @author Ilsur Chubarkin
 * @version 1.0
 */

@Data
@Entity
@Table(name = "account")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "account_sequence")
    @SequenceGenerator(name = "account_sequence", initialValue = 1, allocationSize = 1)
    private Long id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Transient
    private String confirmPassword;
}
