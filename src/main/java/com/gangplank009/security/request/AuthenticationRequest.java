package com.gangplank009.security.request;

/**
 * POJO class represents authentication request, getting from frontend in JSON format
 * @author Ilsur Chubarkin
 * @version 1.0
 */

public class AuthenticationRequest {

    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

